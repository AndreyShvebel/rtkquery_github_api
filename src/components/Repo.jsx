import React, {useState} from 'react'
import { useACtions } from '../hooks/actions';
import { useAppSelector } from '../hooks/redux';

function Repo(props) {
    const {repo} = props;
    const { addFavourite, removeFavourite } = useACtions()
    const {favourites} = useAppSelector(state => state.github)

    const [isFav, setIsFav] = useState(favourites.includes(repo.html_url))

    const addToFav = (e) => {
      e.preventDefault()
      addFavourite(repo.html_url)
      setIsFav(true)
    }

    const removeFromFav = (e) => {
      e.preventDefault()
      removeFavourite(repo.html_url)
      setIsFav(false)
    }
  return (
    <div className='border py-3 px-5 rounded mb-2 hover:shadow-md hover:bg-gray-100 transition-all'>
        <h2 className='text-lg font-bold cursor-pointer'> <a href={repo.html_url} target='_blank' rel='noreferrer' >{repo.full_name}</a> </h2>      
        <p className='font-bold'>
            Forks: <span className='font-bold mr-2'>{repo.forks}</span>
            Watchers: <span className='font-bold mr-2'>{repo.watchers}</span>
        </p>

        {!isFav && <button 
          className='py-2 px-4 bg-yellow-400 mr-2 rounded hover:shadow-md transition-all'
          onClick={addToFav}
        >
          Add 
        </button>}

        {isFav && <button 
          className='py-2 px-4 bg-red-400 rounded hover:shadow-md transition-all'
          onClick={removeFromFav}
        >
          Remove 
        </button>}
    </div>
  )
}

export default Repo
